package ru.tsc.denisturovsky.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static Date nextDate() {

        @NotNull final String value = nextLine();
        return DateUtil.toDate(value);
    }

    @NotNull
    static String nextLine() {

        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {

        @NotNull final String value = nextLine();
        return Integer.parseInt(value);
    }

}

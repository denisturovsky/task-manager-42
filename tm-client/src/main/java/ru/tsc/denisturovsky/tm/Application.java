package ru.tsc.denisturovsky.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.component.Bootstrap;

public final class Application {

    public static void main(@Nullable String[] args) {

        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}

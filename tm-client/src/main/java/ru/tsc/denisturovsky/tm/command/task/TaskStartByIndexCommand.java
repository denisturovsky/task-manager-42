package ru.tsc.denisturovsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.dto.request.TaskStartByIndexRequest;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Start task by index";

    @NotNull
    public static final String NAME = "task-start-by-index";

    @Override
    public void execute() {

        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken());
        request.setIndex(index);
        getTaskEndpoint().startByIndexTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

}

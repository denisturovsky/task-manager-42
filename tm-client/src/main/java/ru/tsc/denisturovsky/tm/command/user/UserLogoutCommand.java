package ru.tsc.denisturovsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.request.UserLogoutRequest;
import ru.tsc.denisturovsky.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Log out";

    @NotNull
    public static final String NAME = "logout";

    @Override
    public void execute() {

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        getAuthEndpoint().logoutUser(request);
        setToken(null);
    }

    @NotNull
    @Override
    public String getDescription() {

        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {

        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {

        return Role.values();
    }

}

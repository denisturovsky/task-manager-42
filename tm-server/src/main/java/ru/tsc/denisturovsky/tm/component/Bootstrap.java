package ru.tsc.denisturovsky.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.endpoint.*;
import ru.tsc.denisturovsky.tm.api.service.*;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.endpoint.*;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.sevice.*;
import ru.tsc.denisturovsky.tm.util.DateUtil;
import ru.tsc.denisturovsky.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, projectService, taskService, propertyService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    {
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    private void initDemoData() {

        try {
            @NotNull final UserDTO user = userService.create("user", "user", "user@user.ru");
            try {
                taskService.add(
                        user.getId(),
                        new TaskDTO(
                                "TASK TEST",
                                Status.IN_PROGRESS,
                                DateUtil.toDate("10.05.2018"),
                                DateUtil.toDate("10.05.2099")
                        )
                );
                taskService.add(
                        user.getId(),
                        new TaskDTO(
                                "TASK BETA",
                                Status.NOT_STARTED,
                                DateUtil.toDate("05.11.2020"),
                                DateUtil.toDate("05.11.2099")
                        )
                );
                taskService.add(
                        user.getId(),
                        new TaskDTO(
                                "TASK BEST",
                                Status.IN_PROGRESS,
                                DateUtil.toDate("15.10.2019"),
                                DateUtil.toDate("15.10.2099")
                        )
                );
                taskService.add(
                        user.getId(),
                        new TaskDTO(
                                "TASK MEGA",
                                Status.COMPLETED,
                                DateUtil.toDate("07.03.2021"),
                                DateUtil.toDate("07.03.2099")
                        )
                );

                projectService.add(
                        user.getId(),
                        new ProjectDTO(
                                "PROJECT DEMO",
                                Status.IN_PROGRESS,
                                DateUtil.toDate("07.05.2018"),
                                DateUtil.toDate("07.05.2099")
                        )
                );
                projectService.add(
                        user.getId(),
                        new ProjectDTO(
                                "PROJECT TEST",
                                Status.NOT_STARTED,
                                DateUtil.toDate("03.11.2020"),
                                DateUtil.toDate("03.11.2099")
                        )
                );
                projectService.add(
                        user.getId(),
                        new ProjectDTO(
                                "PROJECT BEST",
                                Status.IN_PROGRESS,
                                DateUtil.toDate("05.10.2019"),
                                DateUtil.toDate("05.10.2099")
                        )
                );
                projectService.add(
                        user.getId(),
                        new ProjectDTO(
                                "PROJECT MEGA",
                                Status.COMPLETED,
                                DateUtil.toDate("04.03.2021"),
                                DateUtil.toDate("04.03.2099")
                        )
                );
            } catch (@NotNull final Exception e) {
                System.out.println(e.getMessage());
            }
        } catch (@NotNull final Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);
        } catch (@NotNull final Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @SneakyThrows
    private void initPID() {

        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void prepareShutdown() {

        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
    }

    private void registry(@NotNull final Object endpoint) {

        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void run() {

        initPID();
        initDemoData();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}
package ru.tsc.denisturovsky.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.comparator.CreatedComparator;
import ru.tsc.denisturovsky.tm.comparator.DateBeginComparator;
import ru.tsc.denisturovsky.tm.comparator.NameComparator;
import ru.tsc.denisturovsky.tm.comparator.StatusComparator;
import ru.tsc.denisturovsky.tm.exception.field.SortIncorrectException;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

@Getter
public enum Sort {

    BY_NAME("Sort by Name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_BEGIN("Sort by date begin", DateBeginComparator.INSTANCE);

    @NotNull
    private final Comparator comparator;

    @NotNull
    private final String displayName;

    Sort(
            @NotNull String displayName,
            @NotNull Comparator comparator
    ) {

        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static Sort toSort(@NotNull final String value) {

        if (value.isEmpty()) return null;
        @Nullable final Optional<Sort> sortOptional = Arrays.stream(values()).filter(m -> value.equals(m.name())).findFirst();
        if (sortOptional.orElse(null) == null) throw new SortIncorrectException();
        return sortOptional.orElse(null);
    }

}

package ru.tsc.denisturovsky.tm.exception.field;

public class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {

        super("Error! Task Id is empty...");
    }

}

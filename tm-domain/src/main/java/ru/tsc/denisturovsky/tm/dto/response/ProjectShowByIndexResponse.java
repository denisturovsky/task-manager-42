package ru.tsc.denisturovsky.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectShowByIndexResponse extends AbstractProjectResponse {

    public ProjectShowByIndexResponse(@Nullable final ProjectDTO project) {

        super(project);
    }

}
